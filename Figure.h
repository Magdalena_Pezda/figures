/*
 * Figure.h
 *
 *  Created on: 29.03.2017
 *      Author: Magda
 */

#ifndef FIGURE_H_
#define FIGURE_H_

#include <string>

class Figure {
public:
	//constructors
	Figure();
	Figure (std::string name);

	//destructors
	virtual ~Figure();

	//methods
	virtual double getArea()=0;
	virtual void description();
	virtual void draw();
	std::string giveName();

	//attributes
protected:
	std::string name;
	double area;
};

#endif /* FIGURE_H_ */
