/*
 * Rectangle.cpp
 *
 *  Created on: 30.03.2017
 *      Author: Magda
 */
#include <iostream>
#include "Rectangle.h"

//constructors
Rectangle::Rectangle() {
	this->length=4;
	this->width=8;
	this->name="Rectangle";
	this->figureChar='*';
}

Rectangle::Rectangle(double lenght, double width) {
	this->length=lenght;
	this->width=width;
	this->name="Rectangle";
	this->figureChar='*';
}

//destructors
Rectangle::~Rectangle() {
}



void Rectangle::setLength(double n) {
	length=n;
}

double Rectangle::getLength() {
	return length;
}

double Rectangle::getArea() {
	area=length*width;
	std::cout<< area << std::endl;
	return 0;
}

void Rectangle::description() {
	std::cout << " A rectangle is a quadrilateral with four right angles. " << std::endl;
}

std::string Rectangle::giveName() {
	std::cout << name << std::endl;
	return name;
}

void Rectangle::draw() {}

char Rectangle::drawFigure() {

	for (int i=0; i<=length; i++) {
		for (int j=0; j<=width; j++) {
			std::cout<< figureChar;
		}
	std:: cout << "\n";
	}
	return 0;
}
