/*
 * Rectangle.h
 *
 *  Created on: 30.03.2017
 *      Author: Magda
 */

#ifndef RECTANGLE_H_
#define RECTANGLE_H_

#include "Figure.h"

class Rectangle:public Figure {
public:
	//constructors
	Rectangle();
	Rectangle(double lenght, double width);
	void getLenght(double lenght);
	void setLength(double n);
	double getLength();

	//destructors
	virtual ~Rectangle();

	//methods
	double getArea();
	void description();
	void draw();
	std::string giveName();
	char drawFigure();

protected:
	char figureChar;
	double length;
	double width;

};

#endif /* RECTANGLE_H_ */
