/*
 * Triangle.h
 *
 *  Created on: 29.03.2017
 *      Author: Magda
 */

#ifndef TRIANGLE_H_
#define TRIANGLE_H_
#include "Figure.h"

class Triangle:public Figure {
public:
	//constructors
	Triangle();
	Triangle(double base, double high);

	//methods
	double getArea();
	void description();
	void draw();
	std::string giveName();

private:
	double base;
	double high;

};


#endif /* TRIANGLE_H_ */
