/*
 * Square.cpp
 *
 *  Created on: 30.03.2017
 *      Author: Magda
 */

#include "Square.h"
#include <iostream>

//constructors
Square::Square() {
	this->side=4;
	this->name="Square";
	this->figureChar='*';
}

Square::Square(double side) {
	this->side=side;
	this->name="Square";
	this->figureChar='*';
}

//destructors
Square::~Square() {
	// TODO Auto-generated destructor stub
}

//methods
double Square::getArea() {
	area=side*side;
	std::cout<< area << std::endl;
	return 0;
}

void Square::description() {
	std::cout << " In geometry, a square is a regular quadrilateral" << std::endl;
}

std::string Square::giveName() {
	std::cout << name << std::endl;
	return name;
}

void Square::draw() {}


char Square::drawFigure() {

	for (int i=0; i<=side; i++) {
		for (int j=0; j<=side; j++) {
			std::cout<< figureChar;
		}
	std:: cout << "\n";
	}
	return 0;
}
