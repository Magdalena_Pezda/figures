/*
 * Triangle.cpp
 *
 *  Created on: 29.03.2017
 *      Author: Magda
 */

#include "Triangle.h"
#include <iostream>

//constructors
Triangle::Triangle(){
	this->base=4;
	this->high=3;
	this->name="Triangle";
}


Triangle::Triangle(double base, double high) {
	this->base=base;
	this->high=high;
	this->name="Triangle";
}


//methods
double Triangle::getArea() {
	area=1/2*base*high;
	std::cout << area << std::endl;
	return 0;
}

void Triangle::description() {
	std::cout << "A triangle is a polygon with three edges and three vertices. " << std::endl;
}

void Triangle::draw() {
	std::cout << "No draw" << std::endl;
}

std::string Triangle::giveName() {
	std::cout << name << std::endl;
	return name;
}


