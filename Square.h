/*
 * Square.h
 *
 *  Created on: 30.03.2017
 *      Author: Magda
 */

#ifndef SQUARE_H_
#define SQUARE_H_

#include "Rectangle.h"

class Square : public Rectangle {
public:
	//constructors
	Square();
	Square(double side);

	//destructors
	virtual ~Square();

	//methods
	double getArea();
	void description();
	void draw();
	std::string giveName();
	char drawFigure();

	//arguments
protected:
	double side;

};

#endif /* SQUARE_H_ */
