/*
 * Wheel.cpp
 *
 *  Created on: 29.03.2017
 *      Author: Magda
 */
#include <iostream>
#include "Wheel.h"
#include "Figure.h"


//constructors
Wheel::Wheel() :  Figure(name), radius(4), x(0), y(0) {
	name="Wheel";
}
Wheel::Wheel(double radius,double x, double y): radius(radius), x(x), y(y){
	this->name="Wheel";
}

//methods
double Wheel::getArea() {
	area=3.14*radius*radius;
	std::cout << area << std::endl;
	return 0;
}

void Wheel::description() {
	std::cout << "A wheel is a circular component that is intended to rotate on an axle bearing. " << std::endl;
}

void Wheel::draw() {
	std::cout << "No draw" << std::endl;
}

std::string Wheel::giveName() {
	std::cout << name << std::endl;
	return name;
}
