//============================================================================
// Name        : Figures.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

#include "Figure.h"
#include "Wheel.h"
#include "Square.h"
#include "Rectangle.h"
#include "Triangle.h"

int main() {

	cout << "-------Wheel-------" << endl;
	Wheel kolo(3,0,0);
	cout << "Name our figure: ";
	kolo.giveName();
	cout << "Area: ";
	kolo.getArea();
	cout << "Draw: " << endl;
	kolo.draw();
	cout << "Description: ";
	kolo.description();

	cout << "-------Rectangle-------" << endl;
	Rectangle prost(3,4);
	cout << "Name our figure: ";
	prost.giveName();
	cout << "Area: ";
	prost.getArea();
	cout << "Draw: " << endl;
	prost.drawFigure();
	cout << "Description: ";
	prost.description();

	cout << "-------Square-------" << endl;
	Square kw(4);
	cout << "Name our figure: ";
	kw.giveName();
	cout << "Area: ";
	kw.getArea();
	cout << "Draw: " << endl;
	kw.drawFigure();
	cout << "Description: ";
	kw.description();


	cout << "-------Triangle-------" << endl;
	Triangle tr(2,3);
	cout << "Name our figure: ";
	tr.giveName();
	cout << "Area: ";
	tr.getArea();
	cout << "Draw: " << endl;
	tr.draw();
	cout << "Description: ";
	tr.description();




	Square sq;
	Figure *tab[2] = {&kolo, &sq};
	for (int i = 0; i < 2; i++){
		tab[i]->draw();
	}


	return 0;
}

