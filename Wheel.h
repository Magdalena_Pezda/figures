/*
 * Wheel.h
 *
 *  Created on: 29.03.2017
 *      Author: Magda
 */

#ifndef WHEEL_H_
#define WHEEL_H_

#include <string>
#include "Figure.h"

class Wheel:public Figure {
public:
	//constructors
	Wheel();
	Wheel(double radius, double x, double y);

	//destructors
	virtual ~Wheel() {};

	//methods
	double getArea();
	void description();
	void draw();
	std::string giveName();

	//arguments
private:
	double radius; //radius
	double x; //horizontal
	double y; //vertical



};

#endif /* WHEEL_H_ */
